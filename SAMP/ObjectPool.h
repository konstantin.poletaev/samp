#ifndef SAMP_OBJECTPOOL_H
#define SAMP_OBJECTPOOL_H

class CEntity;

namespace SAMP {
	class ObjectPool {
		void *pObjectPool;
		int	  objectSize;
		int	  ofGtaEntity;

	public:
		ObjectPool( void *pObjectPool );

		/// Указатель на количество объектов
		int *pCount = nullptr;
		/**
		 * @brief Массив состояний объектов
		 * @details 0 - не загружен, иначе загружен
		 */
		int *pListed = nullptr;
		// Массив указателей на SA:MP структуры объектов
		void *pObjects = nullptr;

		/**
		 * @brief Возвращает указатель на игровую структуру объекта
		 * @param id id объекта
		 * @return игровая структура объекта
		 */
		CEntity *gtaEntity( short id );
	};

} // namespace SAMP

#endif // SAMP_OBJECTPOOL_H
