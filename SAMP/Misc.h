#ifndef SAMP_MISC_H
#define SAMP_MISC_H

namespace SAMP {
	enum class Cursor : int { hide = 0, show_freeAll, show, show_freeMove, show_lockCamVec };
	class Misc {
		static Misc *self;
		int			 refCounter = 0;

		void *pMisc												 = nullptr;
		int	  ofFpsLimit										 = 0;
		int	  ofCursorMode										 = 0;
		int	  ofCursorCounter									 = 0;
		void( __thiscall *pSwitchCursor )( void *, Cursor, int ) = nullptr;

		Misc();
		~Misc() {}

	public:
		/**
		 * @brief Возвращает инстанс misc SA:MP
		 * @details Если инстанс не существует и параметр \b no_create равен false, то создает новый инстанс
		 * @param no_create Пропустить создание инстанса, если он не существует
		 * @return инстанс misc SA:MP
		 */
		[[nodiscard]] static Misc *Instance( bool no_create = false );
		/**
		 * @brief Возвращает ссылку на инстанс misc SA:MP
		 * @details Поведение подобно вызову метода Instance с аргументами по умолчанию, но дополнительно инкрементируется счетчик ссылок
		 * @return инстанс misc SA:MP
		 */
		[[nodiscard]] static Misc *InstanceRef();
		/**
		 * @brief Удаляет инстанс misc SA:MP
		 * @details Если счетчик ссылок не пустой, то вместо удаления будет декрементирован счетчик. Для принудительного удаления инстанса, используйте force = true
		 * @param force Принудительное удаление
		 */
		static void DeleteInstance( bool force = false );

		/// Возвращает текущее ограничение fps
		int fpsLimit() const;

		/**
		 * @brief Возвращает текущий режим курсора
		 * @details 0 - hide, 1 - show + lock kbd, 2 - show + lock kbd+cam, 3 - show + lock cam, 4 - show + lock cam vec
		 */
		Cursor cursorMode() const;
		/// возвращает счетчик изменений курсора
		int cursorCounter() const;
		/**
		 * @brief Изменение режима курсора
		 * @param mode режим курсора
		 */
		void switchCursor( Cursor mode );
	};

} // namespace SAMP

#endif // SAMP_MISC_H
