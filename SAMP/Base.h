#ifndef SAMPBASE_H
#define SAMPBASE_H

class RakClient;
namespace SAMP {
	class Base {
		static Base *self;
		int refCounter = 0;

		void *pSAMP = nullptr;
		RakClient **ppRakClient = nullptr;
		void **ppPools = nullptr;
		int *pGameState = nullptr;
		void **ppSettings = nullptr;

		int ofNameTagsDistance;
		int ofNoNametagsBehindWalls;
		int ofShowNameTags;
		int ofAddress;
		int ofPort;

		class Pools *_pools = nullptr;
		class Chat *_chat = nullptr;
		class Input *_input = nullptr;
		class Misc *_misc = nullptr;
		class Memory *_mem = nullptr;
		class Dialog *_dialog = nullptr;
		class DXUT *_dxut = nullptr;
		class ScoreBoard *_scoreboard = nullptr;

		Base();
		~Base();

	public:
		/**
		 * @brief Возвращает инстанс SA:MP
		 * @details Если инстанс не существует и параметр \b no_create равен false, то создает новый инстанс
		 * @param no_create Пропустить создание инстанса, если он не существует
		 * @return инстанс SA:MP
		 */
		[[nodiscard]] static Base *Instance( bool no_create = false );
		/**
		 * @brief Возвращает ссылку на инстанс SA:MP
		 * @details Поведение подобно вызову метода Instance с аргументами по умолчанию, но дополнительно инкрементируется счетчик ссылок
		 * @return инстанс SA:MP
		 */
		[[nodiscard]] static Base *InstanceRef();
		/**
		 * @brief Удаляет инстанс SA:MP
		 * @details Если счетчик ссылок не пустой, то вместо удаления будет декрементирован счетчик. Для принудительного удаления инстанса, используйте force = true
		 * @param force Принудительное удаление
		 */
		static void DeleteInstance( bool force = false );

		/**
		 * @brief Проверяет инициализирован ли SAMP
		 * @return true, если инициализирован и можно создать инстанс
		 */
		[[nodiscard]] static bool TestInitilize();

		/**
		 * @brief Возвращает указатель на RakClient
		 * @return указатель на RakClient
		 */
		[[nodiscard]] RakClient *pRakClient();
		/**
		 * @brief Возвращает класс пулов SA:MP
		 * @details Класс создается при первом вызове данного метода
		 * @return класс пулов SA:MP
		 */
		[[nodiscard]] class Pools *Pools();
		/**
		 * @brief Возвращает инстанс чата
		 * @details Эквивалентен вызову SAMP::Chat::Instance(), но внутри функции используется SAMP::Chat::InstanceRef() с записью инстанса в текущий класс
		 * @return инстанс чата SA:MP
		 */
		[[nodiscard]] class Chat *Chat();
		/**
		 * @brief Возвращает инстанс ввода
		 * @details Эквивалентен вызову SAMP::Input::Instance(), но внутри функции используется SAMP::Input::InstanceRef() с записью инстанса в текущий класс
		 * @return инстанс ввода SA:MP
		 */
		[[nodiscard]] class Input *Input();
		/**
		 * @brief Возвращает инстанс misc
		 * @details Эквивалентен вызову SAMP::Misc::Instance(), но внутри функции используется SAMP::Misc::InstanceRef() с записью инстанса в текущий класс
		 * @return инстанс misc SA:MP
		 */
		[[nodiscard]] class Misc *Misc();
		/**
		 * @brief Возвращает инстанс диалога
		 * @details Эквивалентен вызову SAMP::Dialog::Instance(), но внутри функции используется SAMP::Dialog::InstanceRef() с записью инстанса в текущий класс
		 * @return инстанс диалога SA:MP
		 */
		[[nodiscard]] class Dialog *Dialog();
		/**
		 * @brief Возвращает инстанс DXUT
		 * @details Эквивалентен вызову SAMP::DXUT::Instance(), но внутри функции используется SAMP::DXUT::InstanceRef() с записью инстанса в текущий класс
		 * @return инстанс DXUT SA:MP
		 */
		[[nodiscard]] class DXUT *Dxut();

		[[nodiscard]] class ScoreBoard *Scoreboard();

		[[nodiscard]] class Memory *Mem();

		/**
		 * @brief Проверяет подключен ли клиент к серверу
		 * @return true, если подключен, иначе false
		 */
		[[nodiscard]] bool isConnected() const;

		/// Указатель на настройки сервера
		[[nodiscard]] void *pSettings();
		/// Возвращает дистанцию прорисовки ников
		[[nodiscard]] float NameTagsDistance();
		/// Возвращает информацию о том, надо ли скрывать ники за стенами
		[[nodiscard]] bool NoNametagsBehindWalls();
		/// Возвращает информацию о том, надо ли рисовать ники
		[[nodiscard]] bool ShowNameTags();

		/// Возвращает IP сервера
		[[nodiscard]] const char *GetServerIP();
		/// Возвращает порт сервера
		[[nodiscard]] unsigned short GetServerPort();
	};
} // namespace SAMP

#endif // SAMPBASE_H
