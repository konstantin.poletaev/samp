#ifndef SCOREBOARD_H
#define SCOREBOARD_H


class CDXUTDialog;
class CDXUTListBox;

namespace SAMP {
	class ScoreBoard {
		static ScoreBoard *self;
		int			  refCounter = 0;

		void *		   pScoreBoard							= nullptr;
		int			   ofEnabled						= 0;
		int			   ofDialog							= 0;
		int			   ofList							= 0;

		ScoreBoard();
		~ScoreBoard() {}

	public:
		/**
		 * @brief Возвращает инстанс таба SA:MP
		 * @details Если инстанс не существует и параметр \b no_create равен false, то создает новый инстанс
		 * @param no_create Пропустить создание инстанса, если он не существует
		 * @return инстанс таба SA:MP
		 */
		[[nodiscard]] static ScoreBoard *Instance( bool no_create = false );
		/**
		 * @brief Возвращает ссылку на инстанс таба SA:MP
		 * @details Поведение подобно вызову метода Instance с аргументами по умолчанию, но дополнительно инкрементируется счетчик ссылок
		 * @return инстанс таба SA:MP
		 */
		[[nodiscard]] static ScoreBoard *InstanceRef();
		/**
		 * @brief Удаляет инстанс таба SA:MP
		 * @details Если счетчик ссылок не пустой, то вместо удаления будет декрементирован счетчик. Для принудительного удаления инстанса, используйте force = true
		 * @param force Принудительное удаление
		 */
		static void DeleteInstance( bool force = false );

		/// Проверяет, активен ли таб
		[[nodiscard]] bool isEnabled() const;

		/// Возвращает диалог
		[[nodiscard]] CDXUTDialog *dialog() const;

		/// Возвращает поле список игроков
		[[nodiscard]] CDXUTListBox *list() const;
	};

} // namespace SAMP

#endif // SCOREBOARD_H
