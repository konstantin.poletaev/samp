#include "Input.h"
#include "Base.h"
#include "Library.h"
#include <callfunc.hpp>
#include <memsafe.h>
#include <stdexcept>

SAMP::Input *SAMP::Input::self = nullptr;

SAMP::Input::Input() {
	switch ( Version() ) {
		case eVerCode::notLoaded:
			throw std::runtime_error( "SAMP not loaded" );
			break;
		case eVerCode::R1:
			pInput		 = reinterpret_cast<void *>( Library() + 0x21A0E8 );
			pJnzChatT	 = reinterpret_cast<unsigned char *>( Library() + 0x5DB23 );
			pSendCommand = reinterpret_cast<void( __stdcall * )( const char * )>( Library() + 0x65C60 );
			pCloseChat	 = reinterpret_cast<decltype( pCloseChat )>( Library() + 0x658E0 );
			pOpenChat	 = reinterpret_cast<decltype( pCloseChat )>( Library() + 0x657E0 );
			pAddCmd		 = reinterpret_cast<decltype( pAddCmd )>( Library() + 0x65AD0 );
			break;
		case eVerCode::R3:
			pInput		 = reinterpret_cast<void *>( Library() + 0x26E8CC );
			pJnzChatT	 = reinterpret_cast<unsigned char *>( Library() + 0x60EC3 );
			pSendCommand = reinterpret_cast<void( __stdcall * )( const char * )>( Library() + 0x69190 );
			pCloseChat	 = reinterpret_cast<decltype( pCloseChat )>( Library() + 0x68E10 );
			pOpenChat	 = reinterpret_cast<decltype( pCloseChat )>( Library() + 0x68D10 );
			pAddCmd		 = reinterpret_cast<decltype( pAddCmd )>( Library() + 0x69000 );
			break;
		case eVerCode::R4:
			pInput		 = reinterpret_cast<void *>( Library() + 0x26E9FC );
			pJnzChatT	 = reinterpret_cast<unsigned char *>( Library() + 0x615F3 );
			pSendCommand = reinterpret_cast<void( __stdcall * )( const char * )>( Library() + 0x698C0 );
			pCloseChat	 = reinterpret_cast<decltype( pCloseChat )>( Library() + 0x69540 );
			pOpenChat	 = reinterpret_cast<decltype( pCloseChat )>( Library() + 0x69440 );
			pAddCmd		 = reinterpret_cast<decltype( pAddCmd )>( Library() + 0x69730 );
			break;
		default:
			throw std::runtime_error( "Unsupported SAMP version" );
			break;
	}
	ofEnabled		= 0x14E0;
	ofRecall		= 0x1565;
	ofRecallTotal	= 0x1AF4;
	ofRecallCurrent = 0x1AF0;
	recallMaxLen	= 129;
	ofDialog		= 0x04;
	ofEdit			= 0x08;
	ofCmdCount		= 0x14DC;
	ofCmdNames		= 0x24C;
	ofCmdCbs		= 0xC;
	cmdMaxLen		= 33;
	cmdDefaultCount = 15;
	cmdMaxCount		= 144;

	++refCounter;
}

SAMP::Input *SAMP::Input::Instance( bool no_create ) {
	if ( !self && !no_create ) self = new Input();
	return self;
}

SAMP::Input *SAMP::Input::InstanceRef() {
	auto instance = Instance();
	if ( instance != nullptr ) ++instance->refCounter;
	return instance;
}

void SAMP::Input::DeleteInstance( bool force ) {
	if ( !self ) return;
	--self->refCounter;
	if ( !self->refCounter || force ) {
		delete self;
		self = nullptr;
	}
}

bool SAMP::Input::isInputEnabled() const {
	if ( !pInput ) return false;
	return *(int *)( *(size_t *)pInput + ofEnabled );
}

bool SAMP::Input::toggleChatT( bool state ) {
	if ( !pJnzChatT ) return false;
	if ( !state ) {
		if ( !isChatT() ) return false;
		memsafe::write( pJnzChatT, '\xEB' );
	} else {
		if ( isChatT() ) return false;
		memsafe::write( pJnzChatT, '\x75' );
	}
	return true;
}

bool SAMP::Input::isChatT() const {
	if ( !pJnzChatT ) return false;
	if ( *pJnzChatT != 0x75 ) return false;
	return true;
}

int &SAMP::Input::recalls() const {
	static auto dummy = -1;
	if ( !pInput ) return dummy;
	return *(int *)( *(size_t *)pInput + ofRecallTotal );
}

int &SAMP::Input::currentRecall() const {
	static auto dummy = -1;
	if ( !pInput ) return dummy;
	return *(int *)( *(size_t *)pInput + ofRecallCurrent );
}

char *SAMP::Input::recall( int id ) const {
	if ( !pInput ) return nullptr;
	if ( id < 0 ) {
		if ( !recalls() ) return nullptr;
		id = recalls() + id;
		if ( id < 0 ) return nullptr;
	}
	return (char *)( *(size_t *)pInput + ofRecall + recallMaxLen * id );
}

void SAMP::Input::sendCommand( const char *cmd ) {
	if ( !pSendCommand ) return;
	pSendCommand( cmd );
}

void SAMP::Input::sendCommand( std::string_view cmd ) {
	char c_str[129]{ 0 };
	memcpy( c_str, cmd.data(), cmd.length() );
	sendCommand( c_str );
}

CDXUTDialog *SAMP::Input::dialog() const {
	if ( !pInput ) return nullptr;
	return *(CDXUTDialog **)( *(size_t *)pInput + ofDialog );
}

CDXUTEditBox *SAMP::Input::edit() const {
	if ( !pInput ) return nullptr;
	return *(CDXUTEditBox **)( *(size_t *)pInput + ofEdit );
}

int SAMP::Input::cmdCount() const {
	if ( !pInput ) return 0;
	return *(int *)( *(size_t *)pInput + ofCmdCount );
}

char *SAMP::Input::cmd( int id ) const {
	if ( !pInput ) return nullptr;
	if ( id < 0 ) {
		if ( !cmdCount() ) return nullptr;
		id = cmdCount() + id;
		if ( id < 0 ) return nullptr;
	}
	return (char *)( *(size_t *)pInput + ofCmdNames + id * cmdMaxLen );
}

int SAMP::Input::cmdId( std::string_view _cmd ) const {
	if ( !pInput ) return -1;
	for ( int i = cmdDefaultCount; i < cmdCount(); ++i )
		if ( _cmd == cmd( i ) ) return i;
	return -1;
}

bool SAMP::Input::isCmdRegistered( std::string_view _cmd ) const {
	if ( !pInput ) return false;
	if ( cmdId( _cmd ) != -1 ) return true;
	return false;
}

bool SAMP::Input::registerCommand( std::string_view cmd, const command_func_t &cb ) { // NOTE: not tested
	if ( !pInput ) return false;
	if ( !pAddCmd ) return false;
	if ( cb == nullptr ) return false;
	if ( isCmdRegistered( cmd ) ) return false;
	if ( cmdCount() >= cmdMaxCount ) return false;
	pAddCmd( *(void **)pInput, cmd.data(), cb );
	return true;
}

bool SAMP::Input::unregisterCommand( std::string_view cmd ) { // NOTE: not tested
	if ( !pInput ) return false;
	if ( !isCmdRegistered( cmd ) ) return false;
	auto id = cmdId( cmd );
	if ( id == -1 ) return false;
	if ( id == 0 || id == cmdCount() - 1 ) {
		*(char *)( *(size_t *)pInput + ofCmdNames + id * cmdMaxLen ) = '\0';
		*(command_func_t *)( *(size_t *)pInput + ofCmdCbs + id * 4 ) = nullptr;
	} else {
		for ( int i = id + 1; i < cmdMaxCount; ++i ) {
			strcpy( (char *)( *(size_t *)pInput + ofCmdNames + ( i - 1 ) * cmdMaxLen ),
					(char *)( *(size_t *)pInput + ofCmdNames + i * cmdMaxLen ) );
			*(command_func_t *)( *(size_t *)pInput + ofCmdCbs + ( i - 1 ) * 4 ) =
				*(command_func_t *)( *(size_t *)pInput + ofCmdCbs + i * 4 );
		}
	}
	*(int *)( *(size_t *)pInput + ofCmdCount ) -= 1;
	return true;
}

SAMP::Input::command_func_t SAMP::Input::redirectCommand( std::string_view					 cmd,
														  const SAMP::Input::command_func_t &cb ) { // NOTE: not tested
	if ( !pInput ) return nullptr;
	if ( !isCmdRegistered( cmd ) ) return nullptr;
	auto id = cmdId( cmd );
	if ( id == -1 ) return nullptr;
	auto result = *(command_func_t *)( *(size_t *)pInput + ofCmdCbs + id * 4 );
	*(command_func_t *)( *(size_t *)pInput + ofCmdCbs + id * 4 ) = cb;
	return result;
}

bool SAMP::Input::renameCommand( std::string_view cmd, std::string_view new_cmd ) { // NOTE: not tested
	if ( !pInput ) return false;
	if ( !isCmdRegistered( cmd ) ) return false;
	auto id = cmdId( cmd );
	if ( id == -1 ) return false;
	strcpy( (char *)( *(size_t *)pInput + ofCmdNames + id * cmdMaxLen ), new_cmd.data() );
	return true;
}

void SAMP::Input::closeChat() {
	if ( !pInput ) return;
	if ( !pCloseChat ) return;
	pCloseChat( pInput );
}

void SAMP::Input::openChat() {
	if ( !pInput ) return;
	if ( !pOpenChat ) return;
	pOpenChat( pInput );
}

SAMP::Input::Command::Command( SAMP::Input *input ) : _input( input ) {}

SAMP::Input::Command::Command( const std::string &name, SAMP::Input *input ) : _input( input ), _name( name ) {}

SAMP::Input::Command::Command( const std::string &name, const std::function<void( std::string_view )> &cb,
							   SAMP::Input *input )
	: _input( input ), _name( name ), _callback( cb ) {}

SAMP::Input::Command::~Command() {
	if ( _installed ) remove();
	if ( _helper ) delete _helper;
	if ( _input ) {
		Input::DeleteInstance();
		_input = nullptr;
	}
}

bool SAMP::Input::Command::install() {
	if ( _installed ) return false;
	if ( _name.empty() ) return false;
	if ( !SAMP::Base::TestInitilize() ) return false;
	if ( !_input ) _input = Input::InstanceRef();
	if ( _input->isCmdRegistered( _name ) ) return false;

	if ( _helper == nullptr ) _helper = make_helper( this, &Command::command );

	_installed = _input->registerCommand( _name, _helper->c_func() );
	return _installed;
}

bool SAMP::Input::Command::install( const std::string &name ) {
	_name = name;
	return install();
}

bool SAMP::Input::Command::install( const std::string &name, const std::function<void( std::string_view )> &cb ) {
	_name	  = name;
	_callback = cb;
	return install();
}

bool SAMP::Input::Command::install( const std::function<void( std::string_view )> &cb ) {
	_callback = cb;
	return install();
}

bool SAMP::Input::Command::remove() {
	if ( !_installed ) return false;
	if ( !SAMP::Base::TestInitilize() ) return false;
	if ( !_input ) return false;
	if ( !_input->isCmdRegistered( _name ) ) {
		if ( _installed ) {
			_installed = false;
			return true;
		}
		return false;
	}
	if ( !_input->unregisterCommand( _name ) ) return false;
	_installed = false;
	return true;
}

bool SAMP::Input::Command::isInstalled() const {
	return _installed;
}

bool SAMP::Input::Command::rename( const std::string &name ) {
	if ( !_installed ) return false;
	if ( !SAMP::Base::TestInitilize() ) return false;
	if ( !_input ) return false;
	if ( !_input->isCmdRegistered( _name ) ) return false;
	if ( _input->isCmdRegistered( name ) ) return false;
	if ( !_input->renameCommand( _name, name ) ) return false;
	_name = name;
	return true;
}

std::function<void( std::string_view )>
	SAMP::Input::Command::redirect( const std::function<void( std::string_view )> &cb ) {
	auto prevCb = _callback;
	_callback	= cb;
	return prevCb;
}

void SAMP::Input::Command::command( const char *params ) {
	_callback( params );
}
