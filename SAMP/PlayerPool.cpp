#include "PlayerPool.h"
#include "Library.h"
#include "RemotePlayer.h"
#include <stdexcept>
#include <windows.h>

SAMP::PlayerPool::PlayerPool( void *pPlayerPool ) : pPlayerPool( pPlayerPool ) {
	switch ( Version() ) {
		case eVerCode::R1:
			pListed			 = reinterpret_cast<int *>( (size_t)pPlayerPool + 4062 );
			_localPlayerId	 = reinterpret_cast<short *>( (size_t)pPlayerPool + 4 );
			pRemotePlayers	 = (size_t)pPlayerPool + 46;
			_localPlayerNick = (size_t)pPlayerPool + 0x6 + 4;
			break;
		case eVerCode::R4:
			[[fallthrough]];
		case eVerCode::R3:
			pListed			 = reinterpret_cast<int *>( (size_t)pPlayerPool + 4020 );
			_localPlayerId	 = reinterpret_cast<short *>( (size_t)pPlayerPool + 12060 );
			pRemotePlayers	 = (size_t)pPlayerPool + 4;
			_localPlayerNick = (size_t)pPlayerPool + 0x2F1E + 4;
			break;
		default:
			throw std::runtime_error( "Unsupported SAMP version" );
			break;
	}
}

SAMP::PlayerPool::~PlayerPool() {
	if ( _remotePlayers ) delete _remotePlayers;
}


SAMP::RemotePlayer *SAMP::PlayerPool::RemotePlayer( short id ) {
	if ( pRemotePlayers != 0 ) {
		if ( !_remotePlayers ) _remotePlayers = new SAMP::RemotePlayer( pRemotePlayers );
		_remotePlayers->id = id;
		return _remotePlayers;
	}
	return nullptr;
}

short SAMP::PlayerPool::localPlayerId() {
	if ( _localPlayerId ) return *_localPlayerId;
	return -1;
}

std::string SAMP::PlayerPool::localPlayerNick() {
	struct msvcString {
		union {
			char  szString[16];
			char *pszString;
		};
		int length;
		int allocated;
	};
	auto str = *(msvcString *)( _localPlayerNick );
	if ( str.allocated < 0x10 ) return std::string( str.szString, str.length );
	return std::string( str.pszString, str.length );
}
