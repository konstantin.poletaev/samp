#include "Library.h"
#include <windows.h>

unsigned long SAMP::Library() {
	static DWORD samp = 0;
	if ( !samp ) { // Если SA:MP хоть раз загрузился, то больше не насилуем системную функцию
		samp = (DWORD)GetModuleHandleA( "samp" );
		if ( samp == 0xFFFFFFFF ) samp = 0;
	}
	return samp;
}

SAMP::eVerCode SAMP::Version() {
	static eVerCode ver = eVerCode::unknown;
	if ( ver != eVerCode::unknown ) return ver;
	auto samp = Library();
	if ( !samp )
		return eVerCode::notLoaded;
	else if ( *(unsigned char *)( samp + 0x129 ) == 0xF4 )
		ver = eVerCode::R1;
	else if ( *(unsigned char *)( samp + 0x129 ) == 0x0C )
		ver = eVerCode::R2;
	else if ( *(unsigned char *)( samp + 0x129 ) == 0x00 ) {
		if ( *(unsigned char *)( samp + 0x140 ) == 0xD0 )
			ver = eVerCode::R3;
		else if ( *(unsigned char *)( samp + 0x140 ) == 0xB0 )
			ver = eVerCode::R4;
	} else if ( *(unsigned char *)( samp + 0x129 ) == 0x31 )
		ver = eVerCode::DL;
	return ver;
}

bool SAMP::isR1() {
	return Version() == eVerCode::R1;
}

bool SAMP::isR2() {
	return Version() == eVerCode::R2;
}

bool SAMP::isR3() {
	return Version() == eVerCode::R3;
}

bool SAMP::isR4() {
	return Version() == eVerCode::R4;
}

bool SAMP::isDL() {
	return Version() == eVerCode::DL;
}
