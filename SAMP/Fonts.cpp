#include "Fonts.h"
#include "Library.h"
#include <callfunc.hpp>
#include <stdexcept>
#ifdef __clang__
#	include "cp1251.hpp"
#endif
#include <windef.h>

SAMP::Fonts *SAMP::Fonts::self = nullptr;

SAMP::Fonts::Fonts() {
	switch ( Version() ) {
		case eVerCode::notLoaded:
			throw std::runtime_error( "SAMP not loaded" );
			break;
		case eVerCode::R1:
			pFonts = reinterpret_cast<void *>( Library() + 0x21A0FC );
			pMeasureText = reinterpret_cast<void *>( Library() + 0x66B20 );
			pMeasureTextLittle = reinterpret_cast<void *>( Library() + 0x66BD0 );
			pRenderText = reinterpret_cast<void *>( Library() + 0x66C80 );
			pRenderTextLittle = reinterpret_cast<void *>( Library() + 0x66E00 );
			break;
		case eVerCode::R2:
			pFonts = reinterpret_cast<void *>( Library() + 0x21A104 );
			pMeasureText = reinterpret_cast<void *>( Library() + 0x66BF0 );
			pMeasureTextLittle = reinterpret_cast<void *>( Library() + 0x66CA0 );
			pRenderText = reinterpret_cast<void *>( Library() + 0x66D50 );
			pRenderTextLittle = reinterpret_cast<void *>( Library() + 0x66ED0 );
			break;
		case eVerCode::R3:
			pFonts = reinterpret_cast<void *>( Library() + 0x26E8E4 );
			pMeasureText = reinterpret_cast<void *>( Library() + 0x6AA90 );
			pMeasureTextLittle = reinterpret_cast<void *>( Library() + 0x6AB40 );
			pRenderText = reinterpret_cast<void *>( Library() + 0x6ABF0 );
			pRenderTextLittle = reinterpret_cast<void *>( Library() + 0x6AD70 );
			break;
		case eVerCode::R4:
			pFonts = reinterpret_cast<void *>( Library() + 0x26E8E4 );
			pMeasureText = reinterpret_cast<void *>( Library() + 0x6B1C0 );
			pMeasureTextLittle = reinterpret_cast<void *>( Library() + 0x6B270 );
			pRenderText = reinterpret_cast<void *>( Library() + 0x6B320 );
			pRenderTextLittle = reinterpret_cast<void *>( Library() + 0x6B4A0 );
			break;
		case eVerCode::DL:
			pFonts = reinterpret_cast<void *>( Library() + 0x2ACA2C );
			pMeasureText = reinterpret_cast<void *>( Library() + 0x6AC40 );
			pMeasureTextLittle = reinterpret_cast<void *>( Library() + 0x6ACF0 );
			pRenderText = reinterpret_cast<void *>( Library() + 0x6ADA0 );
			pRenderTextLittle = reinterpret_cast<void *>( Library() + 0x6AF20 );
			break;
		default:
			throw std::runtime_error( "Unsupported SAMP version" );
			break;
	}
	ofChatFont = 0;
	ofLittleFont = 4;
	ofChatShadowFont = 8;
	ofLittleShadowFont = 0xC;
	ofChatFontHeight = 0x20;
	ofLittleFontHeight = 0x24;

	++refCounter;
}

SAMP::Fonts *SAMP::Fonts::Instance( bool no_create ) {
	if ( !self && !no_create ) self = new Fonts();
	return self;
}

SAMP::Fonts *SAMP::Fonts::InstanceRef() {
	auto instance = Instance();
	if ( instance != nullptr ) ++instance->refCounter;
	return instance;
}

void SAMP::Fonts::DeleteInstance( bool force ) {
	if ( !self ) return;
	--self->refCounter;
	if ( !self->refCounter || force ) {
		delete self;
		self = nullptr;
	}
}

SAMP::Fonts::size SAMP::Fonts::measureText( const char *text, int format ) const {
	if ( !pFonts ) return {};
	if ( !*(int *)pFonts ) return {};
	if ( !pMeasureText ) return {};
	tagRECT rect{ 0, 0, 0, 0 };
#ifdef __clang__
	auto result = CallFunc::thiscall<RECT *>( *(int *)pFonts, pMeasureText, &rect, cp1251str( text ).data(), format );
#else
	auto result = CallFunc::thiscall<RECT *>( *(int *)pFonts, pMeasureText, &rect, text, format );
#endif
	return { result->left, result->top };
}

SAMP::Fonts::size SAMP::Fonts::measureText( const std::string &text, int format ) const {
	return measureText( text.c_str(), format );
}

SAMP::Fonts::size SAMP::Fonts::measureTextLittle( const char *text, int format ) const {
	if ( !pFonts ) return {};
	if ( !*(int *)pFonts ) return {};
	if ( !pMeasureTextLittle ) return {};
	tagRECT rect{ 0, 0, 0, 0 };
#ifdef __clang__
	auto result = CallFunc::thiscall<RECT *>( *(int *)pFonts, pMeasureTextLittle, &rect, cp1251str( text ).data(), format );
#else
	auto result = CallFunc::thiscall<RECT *>( *(int *)pFonts, pMeasureTextLittle, &rect, text, format );
#endif
	return { result->left, result->top };
}

SAMP::Fonts::size SAMP::Fonts::measureTextLittle( const std::string &text, int format ) const {
	return measureTextLittle( text.c_str(), format );
}

void SAMP::Fonts::drawText( const char *text, const tagRECT &rect, int color, bool border, ID3DXSprite *sprite ) {
	if ( !pFonts ) return;
	if ( !*(int *)pFonts ) return;
	if ( !pRenderText ) return;
#ifdef __clang__
	CallFunc::thiscall( *(int *)pFonts, pRenderText, sprite, cp1251str( text ).data(), rect, color, border );
#else
	CallFunc::thiscall( *(int *)pFonts, pRenderText, sprite, text, rect, color, border );
#endif
}

void SAMP::Fonts::drawText( const std::string &text, const tagRECT &rect, int color, bool border, ID3DXSprite *sprite ) {
	return drawText( text.c_str(), rect, color, border, sprite );
}

void SAMP::Fonts::drawText( int x, int y, const char *text, int color ) {
	tagRECT rect{ x, y, x + 2048, y + 1024 };
	drawText( text, rect, color, false );
}

void SAMP::Fonts::drawText( int x, int y, const std::string &text, int color ) {
	drawText( x, y, text.c_str(), color );
}

void SAMP::Fonts::drawTextLittle( const char *text, const tagRECT &rect, int color, bool border, ID3DXSprite *sprite ) {
	if ( !pFonts ) return;
	if ( !*(int *)pFonts ) return;
	if ( !pRenderTextLittle ) return;
#ifdef __clang__
	CallFunc::thiscall( *(int *)pFonts, pRenderTextLittle, sprite, cp1251str( text ).data(), rect, 0x00000010, color, border );
#else
	CallFunc::thiscall( *(int *)pFonts, pRenderTextLittle, sprite, text, rect, 0x00000010, color, border );
#endif
}

void SAMP::Fonts::drawTextLittle( const std::string &text, const tagRECT &rect, int color, bool border, ID3DXSprite *sprite ) {
	return drawTextLittle( text.c_str(), rect, color, border, sprite );
}

void SAMP::Fonts::drawTextLittle( int x, int y, const char *text, int color ) {
	tagRECT rect{ x, y, x + 2048, y + 1024 };
	drawTextLittle( text, rect, color, false );
}

void SAMP::Fonts::drawTextLittle( int x, int y, const std::string &text, int color ) {
	drawTextLittle( x, y, text.c_str(), color );
}

ID3DXFont *SAMP::Fonts::font() const {
	if ( !pFonts ) return nullptr;
	if ( !*(int *)pFonts ) return nullptr;
	return *(ID3DXFont **)( *(size_t *)pFonts + ofChatFont );
}

ID3DXFont *SAMP::Fonts::fontShadow() const {
	if ( !pFonts ) return nullptr;
	if ( !*(int *)pFonts ) return nullptr;
	return *(ID3DXFont **)( *(size_t *)pFonts + ofChatShadowFont );
}

ID3DXFont *SAMP::Fonts::fontLittle() const {
	if ( !pFonts ) return nullptr;
	if ( !*(int *)pFonts ) return nullptr;
	return *(ID3DXFont **)( *(size_t *)pFonts + ofLittleFont );
}

ID3DXFont *SAMP::Fonts::fontShadowLittle() const {
	if ( !pFonts ) return nullptr;
	if ( !*(int *)pFonts ) return nullptr;
	return *(ID3DXFont **)( *(size_t *)pFonts + ofLittleShadowFont );
}

int SAMP::Fonts::drawHeight() const {
	if ( !pFonts ) return 0;
	if ( !*(int *)pFonts ) return 0;
	return *(int *)( *(size_t *)pFonts + ofChatFontHeight );
}

int SAMP::Fonts::drawHeightLittle() const {
	if ( !pFonts ) return 0;
	if ( !*(int *)pFonts ) return 0;
	return *(int *)( *(size_t *)pFonts + ofLittleFontHeight );
}

int SAMP::Fonts::drawLength( const char *text ) const {
	return measureText( text ).width;
}

int SAMP::Fonts::drawLength( const std::string &text ) const {
	return drawLength( text.c_str() );
}

int SAMP::Fonts::drawLengthLittle( const char *text ) const {
	return measureTextLittle( text ).width;
}

int SAMP::Fonts::drawLengthLittle( const std::string &text ) const {
	return drawLengthLittle( text.c_str() );
}
