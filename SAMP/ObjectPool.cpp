#include <stdexcept>
#include <windows.h>

#include "Library.h"
#include "ObjectPool.h"

SAMP::ObjectPool::ObjectPool( void *pObjectPool ) : pObjectPool( pObjectPool ) {
	switch ( Version() ) {
		case eVerCode::notLoaded:
			throw std::runtime_error( "SAMP not loaded" );
			break;
		case eVerCode::R1:
			pCount		= reinterpret_cast<int *>( (size_t)pObjectPool + 0 );
			pListed		= reinterpret_cast<int *>( (size_t)pObjectPool + 4 );
			pObjects	= reinterpret_cast<void **>( (size_t)pObjectPool + 0xFA4 );
			objectSize	= 0x1199;
			ofGtaEntity = 0x40;
			break;
		case eVerCode::R4:
			[[fallthrough]];
		case eVerCode::R3:
			pCount		= reinterpret_cast<int *>( (size_t)pObjectPool + 0 );
			pListed		= reinterpret_cast<int *>( (size_t)pObjectPool + 4 );
			pObjects	= reinterpret_cast<void *>( (size_t)pObjectPool + 0xFA4 );
			objectSize	= 0x119C;
			ofGtaEntity = 0x40;
			break;
		default:
			throw std::runtime_error( "Unsupported SAMP version" );
			break;
	}
}

CEntity *SAMP::ObjectPool::gtaEntity( short id ) {
	if ( id < 0 || id > 1000 ) return nullptr;
	if ( !pListed[id] ) return nullptr;
	auto object = ( (size_t *)pObjects )[id];
	if ( !object ) return nullptr;
	return *(CEntity **)( object + ofGtaEntity );
}
