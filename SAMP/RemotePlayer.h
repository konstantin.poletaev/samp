#ifndef SAMP_REMOTEPLAYER_H
#define SAMP_REMOTEPLAYER_H

#include <cstdint>

#include <string>
#include <string_view>

class CPed;
namespace SAMP {
	class RemotePlayer {
		friend class PlayerPool;

		unsigned pRemotePlayer = 0;
		int ofRemotePlayerData;
		int ofPlayerId;
		int ofVehicleId;
		int ofSampPed;
		int ofGtaPed;
		int ofAnimId;
		int ofIsNPC;
		int ofScore;
		int ofPing;
		int ofNickStruct;
		int ofShowNameTag;
		int ofArmour;
		int ofHealth;
		int ofAfk;
		int ofTeam;

		RemotePlayer( unsigned pRemotePlayer );
		~RemotePlayer() {}

	public:
		/// Максимальное число игроков в пуле
		int maxPlayers;

		/**
		 * @brief Возвращает id игрока из структур SA:MP
		 * @return id игрока
		 */
		short playerId() const;
		/**
		 * @brief Возвращает id машины игрока
		 * @return id машины
		 */
		short vehicleId() const;
		/**
		 * @brief Возвращает указатель на игровую структуру игрока
		 * @return игровая структура игрока
		 */
		CPed *gtaPed() const;
		/**
		 * @brief Возвращает указатель на SA:MP структуру игрока
		 * @return SA:MP структура игрока
		 */
		void *ped() const;
		/**
		 * @brief Возвращает указатель на id анимации
		 * @return указатель на id анимации
		 */
		int *animId();
		/**
		 * @brief Проверяет, является ли игрок NPC
		 * @return 0, если не NPC
		 */
		int isNPC() const;
		/**
		 * @brief Проверяет нужно ли рисовать ник этого игрока
		 * @return 0, если рисовать ник не нужно
		 */
		int isNickShowed() const;
		/// Возвращает AP игрока
		float armour() const;
		/// Возвращает HP игрока
		float health() const;
		/// Проверяет находится ли игрок в AFK
		bool isAfk() const;
		/**
		 * @brief Устанавливает статус AFK игроку
		 * @param state статус AFK
		 */
		void setAfk( bool state );
		/// Возвращает id команды игрока
		std::uint8_t team() const;
		/**
		 * @brief Устанавдивает id команды игрока
		 * @param team id команды
		 */
		void team( std::uint8_t team );
		/**
		 * @brief Возвращает количество очков игрока (левел, если РП сервер)
		 * @details Информация считывается из структуры без обновления. SA:MP обновляет эту информацию при открытии scoreboard
		 * @return количество очков
		 */
		int score() const;
		/**
		 * @brief Возвращает пинг игрока
		 * @details Информация считывается из структуры без обновления. SA:MP обновляет эту информацию при открытии scoreboard
		 * @return пинг
		 */
		int ping() const;
		/**
		 * @brief Возвращает ник игрока
		 * @return ник
		 */
		std::string nick() const;

		/**
		 * @brief Возвращает указатель на внутреннюю SA:MP структуру игрока
		 * @return внутренняя SA:MP структура игрока
		 */
		void *pRemotePlayerData() const;

	protected:
		/// id игрока, для которого вызываются методы
		short id = -1;
	};
} // namespace SAMP

#endif // SAMP_REMOTEPLAYER_H
