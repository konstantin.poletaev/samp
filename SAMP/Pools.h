#ifndef SAMPPOOLS_H
#define SAMPPOOLS_H

namespace SAMP {
	class Pools {
		void * pPools;
		void **ppVehicle;
		void **ppPlayer;
		void **ppObject;
		void **ppTextdraws;

		class VehiclePool *_vehs	= nullptr;
		class PlayerPool * _players = nullptr;
		class ObjectPool * _objects = nullptr;

	public:
		Pools( void *pools );
		~Pools();

		/// Пул транспорта
		class VehiclePool *VehiclePool();
		/// Пул Игроков
		class PlayerPool *PlayerPool();
		/// Пул объектов
		class ObjectPool *ObjectPool();


		void *pTextdrawPool();
	};
} // namespace SAMP

#endif // SAMPPOOLS_H
