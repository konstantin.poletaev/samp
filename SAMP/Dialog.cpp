#include "Dialog.h"
#include "Library.h"
#include <stdexcept>

SAMP::Dialog *SAMP::Dialog::self = nullptr;

SAMP::Dialog::Dialog() {
	switch ( Version() ) {
		case eVerCode::notLoaded:
			throw std::runtime_error( "SAMP not loaded" );
			break;
		case eVerCode::R1:
			pDialog		 = reinterpret_cast<void *>( Library() + 0x21A0B8 );
			ofIsActive	 = 0x28;
			ofType		 = 0x2C;
			ofId		 = 0x30;
			ofText		 = 0x34;
			ofTitle		 = 0x40;
			ofDxut		 = 0x1C;
			ofServerside = 0x81;
			ofList		 = 0x20;
			ofEdit		 = 0x24;
			ofTextX		 = 4;
			ofTextY		 = 8;
			ofTextW		 = 0x38;
			ofTextH		 = 0x3C;
			ofSizeW		 = 0x0C;
			ofSizeH		 = 0x10;
			break;
		case eVerCode::R3:
			pDialog		 = reinterpret_cast<void *>( Library() + 0x26E898 );
			ofIsActive	 = 0x28;
			ofType		 = 0x2C;
			ofId		 = 0x30;
			ofText		 = 0x34;
			ofTitle		 = 0x40;
			ofDxut		 = 0x1C;
			ofServerside = 0x81;
			ofList		 = 0x20;
			ofEdit		 = 0x24;
			ofTextX		 = 4;
			ofTextY		 = 8;
			ofTextW		 = 0x38;
			ofTextH		 = 0x3C;
			ofSizeW		 = 0x0C;
			ofSizeH		 = 0x10;
			break;
		case eVerCode::R4:
			pDialog		 = reinterpret_cast<void *>( Library() + 0x26E9C8 );
			ofIsActive	 = 0x28;
			ofType		 = 0x2C;
			ofId		 = 0x30;
			ofText		 = 0x34;
			ofTitle		 = 0x40;
			ofDxut		 = 0x1C;
			ofServerside = 0x81;
			ofList		 = 0x20;
			ofEdit		 = 0x24;
			ofTextX		 = 4;
			ofTextY		 = 8;
			ofTextW		 = 0x38;
			ofTextH		 = 0x3C;
			ofSizeW		 = 0x0C;
			ofSizeH		 = 0x10;
			break;
		default:
			throw std::runtime_error( "Unsupported SAMP version" );
			break;
	}
	++refCounter;
}

SAMP::Dialog *SAMP::Dialog::Instance( bool no_create ) {
	if ( !self && !no_create ) self = new Dialog();
	return self;
}

SAMP::Dialog *SAMP::Dialog::InstanceRef() {
	auto instance = Instance();
	if ( instance != nullptr ) ++instance->refCounter;
	return instance;
}

void SAMP::Dialog::DeleteInstance( bool force ) {
	if ( !self ) return;
	--self->refCounter;
	if ( !self->refCounter || force ) {
		delete self;
		self = nullptr;
	}
}

bool SAMP::Dialog::isActive() const {
	if ( !pDialog ) return false;
	return *(int *)( *(size_t *)pDialog + ofIsActive );
}

void SAMP::Dialog::setActive( bool state ) {
	if ( !pDialog ) return;
	*(int *)( *(size_t *)pDialog + ofIsActive ) = state;
}

int SAMP::Dialog::type() const {
	if ( !pDialog ) return false;
	return *(int *)( *(size_t *)pDialog + ofType );
}

bool SAMP::Dialog::isRemote() const {
	if ( !pDialog ) return false;
	return *(int *)( *(size_t *)pDialog + ofServerside );
}

int SAMP::Dialog::id() const {
	if ( !pDialog ) return false;
	return *(int *)( *(size_t *)pDialog + ofId );
}

char *SAMP::Dialog::text() const {
	if ( !pDialog ) return nullptr;
	return *(char **)( *(size_t *)pDialog + ofText );
}

void SAMP::Dialog::setText( char *text ) const {
	if ( !pDialog ) return;
	*(char **)( *(size_t *)pDialog + ofText ) = text;
}

int &SAMP::Dialog::textPosX() {
	static int dummy = 0;
	if ( !pDialog ) return dummy;
	return *(int *)( *(size_t *)pDialog + ofTextX );
}

int &SAMP::Dialog::textPosY() {
	static int dummy = 0;
	if ( !pDialog ) return dummy;
	return *(int *)( *(size_t *)pDialog + ofTextY );
}

int &SAMP::Dialog::textW() {
	static int dummy = 0;
	if ( !pDialog ) return dummy;
	return *(int *)( *(size_t *)pDialog + ofTextW );
}

int &SAMP::Dialog::textH() {
	static int dummy = 0;
	if ( !pDialog ) return dummy;
	return *(int *)( *(size_t *)pDialog + ofTextH );
}

int &SAMP::Dialog::sizeW() {
	static int dummy = 0;
	if ( !pDialog ) return dummy;
	return *(int *)( *(size_t *)pDialog + ofSizeW );
}

int &SAMP::Dialog::sizeH() {
	static int dummy = 0;
	if ( !pDialog ) return dummy;
	return *(int *)( *(size_t *)pDialog + ofSizeH );
}

char *SAMP::Dialog::title() const {
	if ( !pDialog ) return nullptr;
	return (char *)( *(size_t *)pDialog + ofTitle );
}

CDXUTDialog *SAMP::Dialog::dialog() const {
	if ( !pDialog ) return nullptr;
	return *(CDXUTDialog **)( *(size_t *)pDialog + ofDxut );
}

CDXUTListBox *SAMP::Dialog::list() const {
	if ( !pDialog ) return nullptr;
	return *(CDXUTListBox **)( *(size_t *)pDialog + ofList );
}

CDXUTEditBox *SAMP::Dialog::edit() const {
	if ( !pDialog ) return nullptr;
	return *(CDXUTEditBox **)( *(size_t *)pDialog + ofEdit );
}
