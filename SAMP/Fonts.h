#pragma once

#include <string>

struct tagRECT;
#ifdef _MSVC_VER
struct ID3DXSprite;
struct ID3DXFont;
#else
class ID3DXSprite;
class ID3DXFont;
#endif

namespace SAMP {
	class Fonts {
		static Fonts *self;
		int refCounter = 0;

		void *pFonts = nullptr;
		void *pMeasureText = nullptr;
		void *pMeasureTextLittle = nullptr;
		void *pRenderText = nullptr;
		void *pRenderTextLittle = nullptr;
		int ofChatFont = 0;
		int ofLittleFont = 0;
		int ofChatShadowFont = 0;
		int ofLittleShadowFont = 0;
		int ofChatFontHeight = 0;
		int ofLittleFontHeight = 0;

		Fonts();
		~Fonts() {}

	public:
		struct size {
			long width = 0;
			long height = 0;
		};

		/**
		 * @brief Возвращает инстанс Fonts SA:MP
		 * @details Если инстанс не существует и параметр \b no_create равен false, то создает новый инстанс
		 * @param no_create Пропустить создание инстанса, если он не существует
		 * @return инстанс Fonts SA:MP
		 */
		[[nodiscard]] static Fonts *Instance( bool no_create = false );
		/**
		 * @brief Возвращает ссылку на инстанс Fonts SA:MP
		 * @details Поведение подобно вызову метода Instance с аргументами по умолчанию, но дополнительно инкрементируется счетчик ссылок
		 * @return инстанс Fonts SA:MP
		 */
		[[nodiscard]] static Fonts *InstanceRef();
		/**
		 * @brief Удаляет инстанс Fonts SA:MP
		 * @details Если счетчик ссылок не пустой, то вместо удаления будет декрементирован счетчик. Для принудительного удаления инстанса, используйте force = true
		 * @param force Принудительное удаление
		 */
		static void DeleteInstance( bool force = false );

		/**
		 * @brief Считает сколько в пикселях необходимо места на экране для текста
		 * @param text Текст
		 * @param format Дополнительные параметры для расчета размеров
		 * @return SAMP::Fonts::size - размер текста для рендера
		 */
		size measureText( const char *text, int format = 0 ) const;
		/**
		 * @brief Считает сколько в пикселях необходимо места на экране для текста
		 * @param text Текст
		 * @param format Дополнительные параметры для расчета размеров
		 * @return SAMP::Fonts::size - размер текста для рендера
		 */
		size measureText( const std::string &text, int format = 0 ) const;

		/**
		 * @brief Считает сколько в пикселях необходимо места на экране для маленького текста
		 * @param text Текст
		 * @param format Дополнительные параметры для расчета размеров
		 * @return SAMP::Fonts::size - размер текста для рендера
		 */
		size measureTextLittle( const char *text, int format = 0 ) const;
		/**
		 * @brief Считает сколько в пикселях необходимо места на экране для маленького текста
		 * @param text Текст
		 * @param format Дополнительные параметры для расчета размеров
		 * @return SAMP::Fonts::size - размер текста для рендера
		 */
		size measureTextLittle( const std::string &text, int format = 0 ) const;

		/**
		 * @brief Рисует текст на экране
		 * @param text Текст для рисования
		 * @param color Цвет текста
		 * @param border Рисовать черную обводку вокруг текста
		 * @param sprite Спрайт для рисования
		 */
		void drawText( const char *text, const tagRECT &rect, int color = -1, bool border = true, ID3DXSprite *sprite = nullptr );
		/**
		 * @brief Рисует текст на экране
		 * @param text Текст для рисования
		 * @param rect Область рисования текста
		 * @param color Цвет текста
		 * @param border Рисовать черную обводку вокруг текста
		 * @param sprite Спрайт для рисования
		 */
		void drawText( const std::string &text, const tagRECT &rect, int color = -1, bool border = true, ID3DXSprite *sprite = nullptr );

		/**
		 * @brief Рисует текст на экране
		 * @param x Позиция X
		 * @param y Позиция Y
		 * @param text Текст для рисования
		 * @param color Цвет текста
		 */
		void drawText( int x, int y, const char *text, int color = -1 );
		/**
		 * @brief Рисует текст на экране
		 * @param x Позиция X
		 * @param y Позиция Y
		 * @param text Текст для рисования
		 * @param color Цвет текста
		 */
		void drawText( int x, int y, const std::string &text, int color = -1 );

		/**
		 * @brief Рисует маленький текст на экране
		 * @param text Текст для рисования
		 * @param rect Область рисования текста
		 * @param color Цвет текста
		 * @param border Рисовать черную обводку вокруг текста
		 * @param sprite Спрайт для рисования
		 */
		void drawTextLittle( const char *text, const tagRECT &rect, int color = -1, bool border = true, ID3DXSprite *sprite = nullptr );
		/**
		 * @brief Рисует маленький текст на экране
		 * @param text Текст для рисования
		 * @param rect Область рисования текста
		 * @param color Цвет текста
		 * @param border Рисовать черную обводку вокруг текста
		 * @param sprite Спрайт для рисования
		 */
		void drawTextLittle( const std::string &text,
							 const tagRECT &rect,
							 int color = -1,
							 bool border = true,
							 ID3DXSprite *sprite = nullptr );

		/**
		 * @brief Рисует маленький текст на экране
		 * @param x Позиция X
		 * @param y Позиция Y
		 * @param text Текст для рисования
		 * @param color Цвет текста
		 */
		void drawTextLittle( int x, int y, const char *text, int color = -1 );
		/**
		 * @brief Рисует маленький текст на экране
		 * @param x Позиция X
		 * @param y Позиция Y
		 * @param text Текст для рисования
		 * @param color Цвет текста
		 */
		void drawTextLittle( int x, int y, const std::string &text, int color = -1 );

		/// Возвращает шрифт, используемый для рисования обычного текста
		[[nodiscard]] ID3DXFont *font() const;

		/// Возвращает шрифт, используемый для рисования тени обычного текст
		[[nodiscard]] ID3DXFont *fontShadow() const;

		/// Возвращает шрифт, используемый для рисования маленького текста
		[[nodiscard]] ID3DXFont *fontLittle() const;

		/// Возвращает шрифт, используемый для рисования тени маленького текст
		[[nodiscard]] ID3DXFont *fontShadowLittle() const;

		/// Возвращает высоту текста для обычного шрифта
		[[nodiscard]] int drawHeight() const;

		/// Возвращает высоту текста для маленького шрифта
		[[nodiscard]] int drawHeightLittle() const;

		/**
		 * @brief Возвращает длину текста для обычного шрифта
		 * @param text Текст
		 */
		[[nodiscard]] int drawLength( const char *text ) const;
		/**
		 * @brief Возвращает длину текста для обычного шрифта
		 * @param text Текст
		 */
		[[nodiscard]] int drawLength( const std::string &text ) const;

		/**
		 * @brief Возвращает длину текста для маленького шрифта
		 * @param text Текст
		 */
		[[nodiscard]] int drawLengthLittle( const char *text ) const;
		/**
		 * @brief Возвращает длину текста для маленького шрифта
		 * @param text Текст
		 */
		[[nodiscard]] int drawLengthLittle( const std::string &text ) const;
	};

} // namespace SAMP
