#include "Chat.h"
#include "Library.h"
#include <callfunc.hpp>
#include <stdexcept>
#include <windows.h>
#ifdef __clang__
#	include "cp1251.hpp"
#endif

SAMP::Chat *SAMP::Chat::self = nullptr;

SAMP::Chat::Chat() {
	switch ( Version() ) {
		case eVerCode::notLoaded:
			throw std::runtime_error( "SAMP not loaded" );
			break;
		case eVerCode::R1:
			pChat = reinterpret_cast<void *>( Library() + 0x21A0E4 );
			pAddInfoMessage = reinterpret_cast<void *>( Library() + 0x644A0 );
			pAddDebugMessage = reinterpret_cast<void *>( Library() + 0x64520 );
			break;
		case eVerCode::R2:
			pChat = reinterpret_cast<void *>( Library() + 0x21A0EC );
			pAddInfoMessage = reinterpret_cast<void *>( Library() + 0x64570 );
			pAddDebugMessage = reinterpret_cast<void *>( Library() + 0x645F0 );
			break;
		case eVerCode::R3:
			pChat = reinterpret_cast<void *>( Library() + 0x26E8C8 );
			pAddInfoMessage = reinterpret_cast<void *>( Library() + 0x678F0 );
			pAddDebugMessage = reinterpret_cast<void *>( Library() + 0x67970 );
			break;
		case eVerCode::R4:
			pChat = reinterpret_cast<void *>( Library() + 0x26E9F8 );
			pAddInfoMessage = reinterpret_cast<void *>( Library() + 0x68030 );
			pAddDebugMessage = reinterpret_cast<void *>( Library() + 0x680B0 );
			break;
		case eVerCode::DL:
			pChat = reinterpret_cast<void *>( Library() + 0x2ACA10 );
			pAddInfoMessage = reinterpret_cast<void *>( Library() + 0x67AE0 );
			pAddDebugMessage = reinterpret_cast<void *>( Library() + 0x67B60 );
			break;
		default:
			throw std::runtime_error( "Unsupported SAMP version" );
			break;
	}
	ofDialog = 0x116;
	ofScroll = 0x11E;
	ofUpdate = 0x63DA;
	ofChatMode = 8;
	ofChatBottom = 0x12E;
	ofPagesize = 0;
	ofTimestamp = 0xC;
	ofFonts = 0x63A2;
	ofTexture = 0x63BA;
	ofSurface = 0x63BE;
	ofTimestampWidth = 0x63E6;
	ofEntries = 0x132;
	cEntryCount = 0x64;
	cEntrySize = 0xFC;
	ofStringHeight = 0x63E2;

	++refCounter;
}

SAMP::Chat *SAMP::Chat::Instance( bool no_create ) {
	if ( !self && !no_create ) self = new Chat();
	return self;
}

SAMP::Chat *SAMP::Chat::InstanceRef() {
	auto instance = Instance();
	if ( instance != nullptr ) ++instance->refCounter;
	return instance;
}

void SAMP::Chat::DeleteInstance( bool force ) {
	if ( !self ) return;
	--self->refCounter;
	if ( !self->refCounter || force ) {
		delete self;
		self = nullptr;
	}
}

void SAMP::Chat::addMsgInfo( std::string_view msg ) const {
	if ( !pChat ) return;
	if ( !*(int *)pChat ) return;
	if ( !pAddInfoMessage ) return;
#ifdef __clang__
	return CallFunc::ccall<void>( pAddInfoMessage, *(int *)pChat, "%s", cp1251str( msg ).data() );
#else
	return CallFunc::ccall<void>( pAddInfoMessage, *(int *)pChat, "%s", msg.data() );
#endif
}

void SAMP::Chat::addMsgDbg( std::string_view msg ) const {
	if ( !pChat ) return;
	if ( !*(int *)pChat ) return;
	if ( !pAddDebugMessage ) return;
#ifdef __clang__
	return CallFunc::ccall<void>( pAddDebugMessage, *(int *)pChat, "%s", cp1251str( msg ).data() );
#else
	return CallFunc::ccall<void>( pAddDebugMessage, *(int *)pChat, "%s", msg.data() );
#endif
}

CDXUTDialog *SAMP::Chat::dialog() const {
	if ( !pChat ) return nullptr;
	if ( !*(int *)pChat ) return nullptr;
	return *(CDXUTDialog **)( *(size_t *)pChat + ofDialog );
}

CDXUTScrollBar *SAMP::Chat::scroll() const {
	if ( !pChat ) return nullptr;
	if ( !*(int *)pChat ) return nullptr;
	return *(CDXUTScrollBar **)( *(size_t *)pChat + ofScroll );
}

void SAMP::Chat::updateRender() const {
	if ( !pChat ) return;
	if ( !*(int *)pChat ) return;
	*(int *)( *(size_t *)pChat + ofUpdate ) = 1;
}

int SAMP::Chat::chatWinMode() const {
	if ( !pChat ) return -1;
	if ( !*(int *)pChat ) return -1;
	return *(int *)( *(size_t *)pChat + ofChatMode );
}

void SAMP::Chat::chatWinMode( int mode ) {
	if ( !pChat ) return;
	if ( !*(int *)pChat ) return;
	*(int *)( *(size_t *)pChat + ofChatMode ) = mode;
	updateRender();
}

int SAMP::Chat::chatWinBottom() const {
	if ( !pChat ) return -1;
	if ( !*(int *)pChat ) return -1;
	return *(int *)( *(size_t *)pChat + ofChatBottom );
}

int SAMP::Chat::pagesize() const {
	if ( !pChat ) return -1;
	if ( !*(int *)pChat ) return -1;
	return *(int *)( *(size_t *)pChat + ofPagesize );
}

void SAMP::Chat::pagesize( int size ) {
	if ( !pChat ) return;
	if ( !*(int *)pChat ) return;
	*(int *)( *(size_t *)pChat + ofPagesize ) = size;
	updateRender();
}

bool SAMP::Chat::isTimestampEnabled() const {
	if ( !pChat ) return false;
	if ( !*(int *)pChat ) return false;
	return *(bool *)( *(size_t *)pChat + ofTimestamp );
}

void SAMP::Chat::toggleTimestamp( bool state ) {
	if ( !pChat ) return;
	if ( !*(int *)pChat ) return;
	*(bool *)( *(size_t *)pChat + ofTimestamp ) = state;
	updateRender();
}

void *SAMP::Chat::fonts() const {
	if ( !pChat ) return nullptr;
	if ( !*(int *)pChat ) return nullptr;
	return *(void **)( *(size_t *)pChat + ofFonts );
}

IDirect3DTexture9 *SAMP::Chat::texture() const {
	if ( !pChat ) return nullptr;
	if ( !*(int *)pChat ) return nullptr;
	return *(IDirect3DTexture9 **)( *(size_t *)pChat + ofTexture );
}

IDirect3DSurface9 *SAMP::Chat::surface() const {
	if ( !pChat ) return nullptr;
	if ( !*(int *)pChat ) return nullptr;
	return *(IDirect3DSurface9 **)( *(size_t *)pChat + ofSurface );
}

int SAMP::Chat::timestampWidth() const {
	if ( !pChat ) return -1;
	if ( !*(int *)pChat ) return -1;
	return *(int *)( *(size_t *)pChat + ofTimestampWidth );
}

void SAMP::Chat::timestampWidth( int size ) {
	if ( !pChat ) return;
	if ( !*(int *)pChat ) return;
	*(int *)( *(size_t *)pChat + ofTimestampWidth ) = size;
	updateRender();
}

void *SAMP::Chat::entry( int id ) const {
	if ( id < 0 || id > cEntryCount ) return nullptr;
	if ( !pChat ) return nullptr;
	if ( !*(int *)pChat ) return nullptr;
	return (void *)( *(size_t *)pChat + ofEntries + id * cEntrySize );
}

int SAMP::Chat::entryCount() const {
	return cEntryCount;
}

int SAMP::Chat::stringHeight() const {
	if ( !pChat ) return -1;
	if ( !*(int *)pChat ) return -1;
	return *(int *)( *(size_t *)pChat + ofStringHeight );
}
