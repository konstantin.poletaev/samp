#ifndef SAMP_LIBRARY_H
#define SAMP_LIBRARY_H

namespace SAMP {
	enum class eVerCode : int
	{
		notLoaded = -1,
		unknown = 0,
		R1,
		R2,
		R3,
		R4,
		DL
	};

	/**
	 * @brief Адрес библиотеки SA:MP
	 * @return не 0, если библиотека загружена
	 */
	[[nodiscard]] unsigned long Library();
	/**
	 * @brief Версия SA:MP 0.3.7 и 0.3DL
	 * @return eVerCode
	 */
	[[nodiscard]] eVerCode Version();

	[[nodiscard]] bool isR1();
	[[nodiscard]] bool isR2();
	[[nodiscard]] bool isR3();
	[[nodiscard]] bool isR4();
	[[nodiscard]] bool isDL();
} // namespace SAMP

#endif // SAMP_LIBRARY_H
