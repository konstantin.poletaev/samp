#include "Base.h"
#include "Chat.h"
#include "DXUT.h"
#include "Dialog.h"
#include "Input.h"
#include "Library.h"
#include "Memory.h"
#include "Misc.h"
#include "Pools.h"
#include "ScoreBoard.h"
#include <stdexcept>
#include <windows.h>

SAMP::Base *SAMP::Base::self = nullptr;

SAMP::Base::Base() {
	switch ( Version() ) {
		case eVerCode::notLoaded:
			throw std::runtime_error( "SAMP not loaded" );
			break;
		case eVerCode::R1:
			pSAMP = *reinterpret_cast<void **>( Library() + 0x21A0F8 );
			ppRakClient = reinterpret_cast<RakClient **>( (size_t)pSAMP + 969 );
			ppPools = reinterpret_cast<void **>( (size_t)pSAMP + 973 );
			pGameState = reinterpret_cast<int *>( (size_t)pSAMP + 0x3BD );
			ppSettings = reinterpret_cast<void **>( (size_t)pSAMP + 0x3C5 );
			ofNameTagsDistance = 0x27;
			ofNoNametagsBehindWalls = 0x2F;
			ofShowNameTags = 0x38;
			ofAddress = 0x20;
			ofPort = 0x225;
			break;
		case eVerCode::R3:
			pSAMP = *reinterpret_cast<void **>( Library() + 0x26E8DC );
			ppRakClient = reinterpret_cast<RakClient **>( (size_t)pSAMP + 44 );
			ppPools = reinterpret_cast<void **>( (size_t)pSAMP + 990 );
			pGameState = reinterpret_cast<int *>( (size_t)pSAMP + 0x3CD );
			ppSettings = reinterpret_cast<void **>( (size_t)pSAMP + 0x3D5 );
			ofNameTagsDistance = 0x27;
			ofNoNametagsBehindWalls = 0x2F;
			ofShowNameTags = 0x38;
			ofAddress = 0x30;
			ofPort = 0x235;
			break;
		case eVerCode::R4:
			pSAMP = *reinterpret_cast<void **>( Library() + 0x26EA0C );
			ppRakClient = reinterpret_cast<RakClient **>( (size_t)pSAMP + 44 );
			ppPools = reinterpret_cast<void **>( (size_t)pSAMP + 990 );
			pGameState = reinterpret_cast<int *>( (size_t)pSAMP + 0x3CD );
			ppSettings = reinterpret_cast<void **>( (size_t)pSAMP + 0x3D5 );
			ofNameTagsDistance = 0x27;
			ofNoNametagsBehindWalls = 0x2F;
			ofShowNameTags = 0x38;
			ofAddress = 0x30;
			ofPort = 0x235;
			break;
		default:
			throw std::runtime_error( "Unsupported SAMP version" );
			break;
	}
}

SAMP::Base::~Base() {
	if ( _pools ) delete _pools;
	if ( _chat ) Chat::DeleteInstance();
	if ( _input ) Input::DeleteInstance();
	if ( _misc ) Misc::DeleteInstance();
	if ( _dialog ) Dialog::DeleteInstance();
	if ( _dxut ) DXUT::DeleteInstance();
	if ( _scoreboard ) DXUT::DeleteInstance();
}

SAMP::Base *SAMP::Base::Instance( bool no_create ) {
	if ( !self && !no_create ) self = new Base();
	return self;
}

SAMP::Base *SAMP::Base::InstanceRef() {
	auto instance = Instance();
	if ( instance != nullptr ) ++instance->refCounter;
	return instance;
}

void SAMP::Base::DeleteInstance( bool force ) {
	if ( !self ) return;
	--self->refCounter;
	if ( !self->refCounter || force ) {
		delete self;
		self = nullptr;
	}
}

bool SAMP::Base::TestInitilize() {
	if ( Version() == eVerCode::R1 )
		return *reinterpret_cast<void **>( Library() + 0x21A0F8 ) != nullptr;
	else if ( Version() == eVerCode::R3 )
		return *reinterpret_cast<void **>( Library() + 0x26E8DC ) != nullptr;
	else if ( Version() == eVerCode::R4 )
		return *reinterpret_cast<void **>( Library() + 0x26EA0C ) != nullptr;
	return false;
}

RakClient *SAMP::Base::pRakClient() {
	if ( ppRakClient == nullptr ) return nullptr;
	return *ppRakClient;
}

SAMP::Pools *SAMP::Base::Pools() {
	if ( ppPools != nullptr ) {
		if ( *ppPools != nullptr ) {
			if ( !_pools ) _pools = new SAMP::Pools( *ppPools );
			return _pools;
		}
	}
	return nullptr;
}

SAMP::Chat *SAMP::Base::Chat() {
	if ( _chat == nullptr ) {
		try {
			_chat = Chat::InstanceRef();
		} catch ( ... ) {}
	}
	return _chat;
}

SAMP::Input *SAMP::Base::Input() {
	if ( _input == nullptr ) {
		try {
			_input = Input::InstanceRef();
		} catch ( ... ) {}
	}
	return _input;
}

SAMP::Misc *SAMP::Base::Misc() {
	if ( _misc == nullptr ) {
		try {
			_misc = Misc::InstanceRef();
		} catch ( ... ) {}
	}
	return _misc;
}

SAMP::Dialog *SAMP::Base::Dialog() {
	if ( _dialog == nullptr ) {
		try {
			_dialog = Dialog::InstanceRef();
		} catch ( ... ) {}
	}
	return _dialog;
}

SAMP::DXUT *SAMP::Base::Dxut() {
	if ( _dxut == nullptr ) {
		try {
			_dxut = DXUT::InstanceRef();
		} catch ( ... ) {}
	}
	return _dxut;
}

SAMP::ScoreBoard *SAMP::Base::Scoreboard() {
	if ( _scoreboard == nullptr ) {
		try {
			_scoreboard = ScoreBoard::InstanceRef();
		} catch ( ... ) {}
	}
	return _scoreboard;
}

SAMP::Memory *SAMP::Base::Mem() {
	if ( _mem == nullptr ) {
		try {
			_mem = Memory::InstanceRef();
		} catch ( ... ) {}
	}
	return _mem;
}

bool SAMP::Base::isConnected() const {
	return *pGameState == ( Version() == eVerCode::R3 ? 5 : 14 );
}

void *SAMP::Base::pSettings() {
	if ( ppSettings ) return *ppSettings;
	return nullptr;
}

float SAMP::Base::NameTagsDistance() {
	auto sets = pSettings();
	if ( !sets ) return -1;
	return *(float *)( (size_t)sets + ofNameTagsDistance );
}

bool SAMP::Base::NoNametagsBehindWalls() {
	auto sets = pSettings();
	if ( !sets ) return false;
	return *(bool *)( (size_t)sets + ofNoNametagsBehindWalls );
}

bool SAMP::Base::ShowNameTags() {
	auto sets = pSettings();
	if ( !sets ) return false;
	return *(bool *)( (size_t)sets + ofShowNameTags );
}

const char *SAMP::Base::GetServerIP() {
	if ( !pSAMP ) return nullptr;
	return (const char *)( (size_t)pSAMP + ofAddress );
}

unsigned short SAMP::Base::GetServerPort() {
	if ( !pSAMP ) return -1;
	return *(unsigned short *)( (size_t)pSAMP + ofPort );
}
